# -----------------------------
# general
# -----------------------------
alias cp="cp -iv"                           # Preferred 'cp' implementation
alias mv="mv -iv"                           # Preferred 'mv' implementation
alias mkdir="mkdir -pv"                     # Preferred 'mkdir' implementation
alias ll="ls -FGlAhp"                       # Preferred 'ls' implementation
# alias less="less -FSRXc"                  # Preferred 'less' implementation
# cd() { builtin cd "$@"; ll; }             # Always list directory contents upon 'cd'
alias cd..="cd ../"                         # Go back 1 directory level (for fast typers)
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias f="open -a Finder ./"                 # f:            Opens current directory in MacOS Finder
alias ~="cd ~"                              # ~:            Go Home
alias c="clear"                             # c:            Clear terminal display
# alias which="type -all"                   # which:        Find executables
alias path="echo -e ${PATH//:/\\n}"         # path:         Echo all executable Paths
# alias show_options="shopt"                # Show_options: display bash options settings
# alias fix_stty="stty sane"                # fix_stty:     Restore terminal settings when screwed up
# alias cic="set completion-ignore-case On" # cic:          Make tab-completion case-insensitive
# mcd () { mkdir -p "$1" && cd "$1"; }      # mcd:          Makes new Dir and jumps inside
# trash () { command mv "$@" ~/.Trash ; }   # trash:        Moves a file to the MacOS trash
# ql () { qlmanage -p "$*" >& /dev/null; }  # ql:           Opens any file in MacOS Quicklook Preview
# alias DT="tee ~/Desktop/terminalOut.txt"  # DT:           Pipe content to file on MacOS Desktop

# `o` with no arguments opens the current directory, otherwise opens the given
# location
function o() {
	if [ $# -eq 0 ]; then
		open .;
	else
		open "$@";
	fi;
}

# List Files & Folders better
alias list='ls -ilha'
alias la="ls -aF"
alias ld="ls -ld"
alias ll="ls -l"
alias lt='ls -At1 && echo "------Oldest--"'
alias ltr='ls -Art1 && echo "------Newest--"'

# Copy the working directory path
alias cpwd='pwd|tr -d "\n"|pbcopy'

# -----------------------------
# system
# -----------------------------
# Clean up LaunchServices to remove duplicates in the “Open With” menu
alias lscleanup="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

# Empty the Trash on all mounted volumes and the main HDD.
# Also, clear Apple’s System Logs to improve shell startup speed.
# Finally, clear download history from quarantine. https://mths.be/bum
alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; sudo rm -rfv /private/var/log/asl/*.asl; sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"

# Recursively delete `.DS_Store` files
alias cleanup="find . -type f -name '*.DS_Store' -ls -delete"

# Hide or show all desktop icons, except those in finder preferences
# Psst This doesn't work when hidden files are shown
alias hidedesktop="chflags hidden ~/Desktop/* & hidef"
alias showdesktop="chflags nohidden ~/Desktop/*"

# Show/hide hidden files in Finder
alias showf="defaults write com.apple.finder AppleShowAllFiles -bool true && killall Finder"
alias hidef="defaults write com.apple.finder AppleShowAllFiles -bool false && killall Finder"

# Lock the screen (when going AFK)
alias afk="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"

# -----------------------------
# git
# -----------------------------
alias gs="git status"
alias g="git"

# -----------------------------
# private
# -----------------------------
alias dotfiles.cd="cd ~/.dotfiles"

alias ak.cd="cd ~/git/gitlab/akaemmerle/akaemmerle.gitlab.io"
alias ak.start="cd ~/git/gitlab/akaemmerle/akaemmerle.gitlab.io && bundle exec jekyll serve"
